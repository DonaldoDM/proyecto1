<?php
	
	session_start();
	
	$usuario = $_SESSION['username'];

    if(!isset($usuario)){
        header('location: login.php');
    }
	
    

    

?>
<!DOCTYPE html>
<!-- Created by CodingLab |www.youtube.com/c/CodingLabYT-->
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <title>Sistema </title>
    <link rel="stylesheet" href="../css/style_sliderbar.css">
    <!-- Boxicons CDN Link -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>

    <script src="https://unpkg.com/boxicons@2.0.9/dist/boxicons.js"></script>
 

    <link rel="stylesheet" href="../css/style_usuarios.css">


    <link rel="stylesheet" href="../css/card_glass--style.css">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
   </head>
<body>
  <div class="sidebar">
    <div class="logo-details">
    <i class='bx bx-ghost bx-tada bx-rotate-90 icon'></i>
        <div class="logo_name">Inventario System</div>
        <i class='bx bx-menu' id="btn" ></i>
    </div>
    <ul class="nav-list">
      <li>
          <i class='bx bx-search' ></i>
         <input type="text" placeholder="Search...">
         <span class="tooltip">Search</span>
      </li>
      <li>
        <a href="main.php">
          <i class='bx bx-grid-alt'></i>
          <span class="links_name">Dashboard</span>
        </a>
         <span class="tooltip">Dashboard</span>
      </li>
      <li>
       <a href="usuarios.php">
         <i class='bx bx-user' ></i>
         <span class="links_name">Usuarios</span>
       </a>
       <span class="tooltip">Usuarios</span>
     </li>
     <li>
       <a href="productos.php">
       <i class='bx bxs-component'></i>
         <span class="links_name">Productos</span>
       </a>
       <span class="tooltip">Productos</span>
     </li>
     <li>
       <a href="envios.php">
         <i class='bx bx-pie-chart-alt-2' ></i>
         <span class="links_name">Envios</span>
       </a>
       <span class="tooltip">Envios</span>
     </li>
     <li>
       <a href="#">
         <i class='bx bx-folder' ></i>
         <span class="links_name">Cajas</span>
       </a>
       <span class="tooltip">Cajas</span>
     </li>
   
     <li class="profile">
         <div class="profile-details">
           <img src="../img/avatar_usuer.svg" alt="profileImg">
           <div class="name_job">
             <div class="name"><?php  echo $usuario; ?></div>
             <div class="job">Conectado</div>
           </div>
         </div>
         <a  id="exit" href="../action/logout.php"><i class='bx bx-log-out' id="log_out" > </i></a>
     </li>
    </ul>
  </div>
  <section class="home-section">
 
  <section class="grid--card">
  <div class="text">USUARIOS</div>
  <div class="glass tm-usuarios">
     <form style="justify-content: center;" action="../action/sp_insert_usuario.php" method="POST">
         <h2>CREAR USUARIO</h2>
        <label>Usuario: </label>
     <input class="input-style" type="text" name="txtUsuario" id="" placeholder="usuario" require>
     <label>Contraseña: </label>
      <input class="input-style" type="passsword" name="txtPass" id="" placeholder="contraseña" require>
     <div style="justify-content: center;">
     <button class="btn-ghost yellow round" type="submit">CREAR</button>
     </div>
     </form>
  </div>
  <div class="glass tm-usuarios">
     <form action="../action/sp_eliminar_user.php" method="POST">
         <h2>ELIMINAR USUARIO</h2>
        <label>Id Usuario: </label>
     <input class="input-style" type="text" name="txtId" id="" placeholder="usuario" require>

     <div style="justify-content: center;">
     <button style="font-size: 50px;" class="btn-ghost red round" type="submit"><i class='bx bxs-trash bx-tada'></i></button>
     </div>
     </form>
  </div>
  </section>


  <section>
  <!--for demo wrap-->
  <h1>Tabla de Usuarios</h1>
  <div class="tbl-header">
    <table cellpadding="0" cellspacing="0" border="0">
      <thead>
        <tr>
          <th>Id</th>
          <th>Usuario</th>
          <th>Contraseña</th>
        </tr>
      </thead>
    </table>
  </div>
  <div class="tbl-content">
    <table cellpadding="0" cellspacing="0" border="0">
      <tbody>
        <?php 
        require '../action/conection.php';
        
        $consulta = mysqli_query($db, "SELECT * FROM login");

        while($mostrar = mysqli_fetch_array($consulta)){
        
        
      
        ?>

     <tr>
       <td><?php 
       echo $mostrar["id"] ?></td>
       <td><?php echo $mostrar["usuario"] ?></td>
       <td><?php echo $mostrar["pass"] ?></td>
     </tr>
     <?php 
     }
     ?>

      </tbody>
    </table>
  </div>
</section>
    

  </section>

  <script src="../js/script_slidebar.js"></script>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

</body>
</html>
