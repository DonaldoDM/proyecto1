<?php
	
	session_start();
	
	$usuario = $_SESSION['username'];

    if(!isset($usuario)){
        header('location: login.php');
    }
	
    require '../action/conection.php';

    $query_num_ussers= mysqli_query($db,"SELECT COUNT(*) FROM login");
    $num_usser = mysqli_fetch_row($query_num_ussers);

    $query_num_prod = mysqli_query($db, "SELECT COUNT(*) FROM productos");
    $num_prod = mysqli_fetch_row($query_num_prod);

    $query_num_sends = mysqli_query($db, "SELECT COUNT(*) FROM envios");
    $num_sends = mysqli_fetch_row($query_num_sends);

?>
<!DOCTYPE html>
<!-- Created by CodingLab |www.youtube.com/c/CodingLabYT-->
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <title>Sistema </title>
    <link rel="stylesheet" href="../css/style_sliderbar.css">
    <!-- Boxicons CDN Link -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>

    <script src="https://unpkg.com/boxicons@2.0.9/dist/boxicons.js"></script>
 

    <link rel="stylesheet" href="../css/style_usuarios.css">


    <link rel="stylesheet" href="../css/card_glass--style.css">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
   </head>
<body>
  <div class="sidebar">
    <div class="logo-details">
    <i class='bx bx-ghost bx-tada bx-rotate-90 icon'></i>
        <div class="logo_name">Invent System</div>
        <i class='bx bx-menu' id="btn" ></i>
    </div>
    <ul class="nav-list">
      <li>
          <i class='bx bx-search' ></i>
         <input type="text" placeholder="Search...">
         <span class="tooltip">Search</span>
      </li>
      <li>
        <a href="main.php">
          <i class='bx bx-grid-alt'></i>
          <span class="links_name">Dashboard</span>
        </a>
         <span class="tooltip">Dashboard</span>
      </li>
      <li>
       <a href="usuarios.php">
         <i class='bx bx-user' ></i>
         <span class="links_name">Usuarios</span>
       </a>
       <span class="tooltip">Usuarios</span>
     </li>
     <li>
     <a href="productos.php">
       <i class='bx bxs-component'></i>
         <span class="links_name">Productos</span>
       </a>
       <span class="tooltip">Productos</span>
     </li>
     <li>
       <a href="envios.php">
         <i class='bx bx-pie-chart-alt-2' ></i>
         <span class="links_name">Envios</span>
       </a>
       <span class="tooltip">Envios</span>
     </li>
     <li>
       <a href="#">
         <i class='bx bx-folder' ></i>
         <span class="links_name">Cajas</span>
       </a>
       <span class="tooltip">Cajas</span>
     </li>
    
     <li class="profile">
         <div class="profile-details">
           <img src="../img/avatar_usuer.svg" alt="profileImg">
           <div class="name_job">
             <div class="name"><?php  echo $usuario; ?></div>
             <div class="job">Conectado</div>
           </div>
         </div>
         <a  id="exit" href="../action/logout.php"><i class='bx bx-log-out' id="log_out" > </i></a>
     </li>
    </ul>
  </div>
  <section class="home-section">
      <div class="text">Envios</div>

      <section>
  <!--for demo wrap-->
  <h1>Tabla Envios</h1>
  <div class="tbl-header">
    <table cellpadding="0" cellspacing="0" border="0">
      <thead>
        <tr>
          <th>Id</th>
          <th>Nombre Destino</th>
          <th>Lugar</th>
          <th>Fecha</th>
          <th>Articulo</th>
          <th>Precio</th>
        </tr>
      </thead>
    </table>
  </div>
  <div class="tbl-content">
    <table cellpadding="0" cellspacing="0" border="0">
      <tbody>
        <?php 
        require '../action/conection.php';
        
        $consulta = mysqli_query($db, "SELECT * FROM envios");

        while($mostrar = mysqli_fetch_array($consulta)){
        
        
      
        ?>

     <tr>
       <td><?php echo $mostrar["id"] ?></td>
       <td><?php echo $mostrar["nombre"] ?></td>
       <td><?php echo $mostrar["lugar"] ?></td>
       <td><?php echo $mostrar["dia_entrega"] ?></td>
       <td><?php echo $mostrar["articulo"] ?></td>
       <td><?php echo $mostrar["precio"] ?></td>



     </tr>
     <?php 
     }
     ?>

      </tbody>
    </table>
  </div>
</section>

      
  </section>

  <script src="../js/script_slidebar.js"></script>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

</body>
</html>
