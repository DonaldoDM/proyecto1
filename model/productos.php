<?php
	
	session_start();
	
	$usuario = $_SESSION['username'];

    if(!isset($usuario)){
        header('location: login.php');
    }
	
    require '../action/conection.php';

?>
<!DOCTYPE html>
<!-- Created by CodingLab |www.youtube.com/c/CodingLabYT-->
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <title>Sistema </title>
    <link rel="stylesheet" href="../css/style_sliderbar.css">
    <!-- Boxicons CDN Link -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>

    <script src="https://unpkg.com/boxicons@2.0.9/dist/boxicons.js"></script>
 

    <link rel="stylesheet" href="../css/style_usuarios.css">
    


    <link rel="stylesheet" href="../css/card_glass--style.css">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
   </head>
<body>
  <div class="sidebar">
    <div class="logo-details">
    <i class='bx bx-ghost bx-tada bx-rotate-90 icon'></i>
        <div class="logo_name">Invent System</div>
        <i class='bx bx-menu' id="btn" ></i>
    </div>
    <ul class="nav-list">
      <li>
          <i class='bx bx-search' ></i>
         <input type="text" placeholder="Search...">
         <span class="tooltip">Search</span>
      </li>
      <li>
        <a href="main.php">
          <i class='bx bx-grid-alt'></i>
          <span class="links_name">Dashboard</span>
        </a>
         <span class="tooltip">Dashboard</span>
      </li>
      <li>
       <a href="usuarios.php">
         <i class='bx bx-user' ></i>
         <span class="links_name">Usuarios</span>
       </a>
       <span class="tooltip">Usuarios</span>
     </li>
     <li>
     <a href="productos.php">
       <i class='bx bxs-component'></i>
         <span class="links_name">Productos</span>
       </a>
       <span class="tooltip">Productos</span>
     </li>
     <li>
       <a href="envios.php">
         <i class='bx bx-pie-chart-alt-2' ></i>
         <span class="links_name">Envios</span>
       </a>
       <span class="tooltip">Envios</span>
     </li>
     <li>
       <a href="#">
         <i class='bx bx-folder' ></i>
         <span class="links_name">Cajas</span>
       </a>
       <span class="tooltip">Cajas</span>
     </li>
    
    
     
     <li class="profile">
         <div class="profile-details">
           <img src="../img/avatar_usuer.svg" alt="profileImg">
           <div class="name_job">
             <div class="name"><?php  echo $usuario; ?></div>
             <div class="job">Conectado</div>
           </div>
         </div>
         <a  id="exit" href="../action/logout.php"><i class='bx bx-log-out' id="log_out" > </i></a>
     </li>
    </ul>
  </div>
  <section class="home-section">
      <div class="text">Productos</div>
    
      <section class="grid--producto ">
        <div class="glass tm--producto">
            <h2>Insertar Productos</h2>
    <form action="../action/insert_producto.php" method="POST">
        <label>Nombre Producto: </label>
        <input class="input-style" type="text" name="txtProd" id="" placeholder="Producto" require>
        <label>Descripcion: </label>
        <input class="input-style" type="text" name="txtDescrip" id="" placeholder="Descripcion" require>
        <label>Precio: </label>
        <input class="input-style" type="text" name="txtPrecio" id="" placeholder="Precio" require>
        <label>Stock: </label>
        <input class="input-style" type="text" name="txtStock" id="" placeholder="Stock" require>

     <div style="justify-content: center;">
     <button class="btn-ghost green round" type="submit">CREAR</button>
     </div>
     </form>
        </div>
      <!-- Editar -->
        <div class="glass tm--producto">
            <h2>Editar Productos</h2>
    <form action="../action/mod_producto.php" method="POST">
      <label>Id Producto: </label>
        <input class="input-style" type="text" name="txtId" id="" placeholder="ID" require>
        <label>Nombre Producto: </label>
        <input class="input-style" type="text" name="txtProd" id="" placeholder="Producto" require>
        <label>Descripcion: </label>
        <input class="input-style" type="text" name="txtDescrip" id="" placeholder="Descripcion" require>
        <label>Precio: </label>
        <input class="input-style" type="text" name="txtPrecio" id="" placeholder="Precio" require>
        <label>Stock: </label>
        <input class="input-style" type="text" name="txtStock" id="" placeholder="Stock" require>

     <div style="justify-content: center;">
     <button class="btn-ghost yellow round" type="submit">Editar</button>
     </div>
     </form>
        </div>
      <!-- delete -->
        <div class="glass " style="width: 600px;">
            <h2>Eliminar Producto</h2>
    <form action="../action/eliminar_producto.php" method="POST">
      <label>Id Producto: </label>
        <input class="input-style" type="text" name="txtId" id="" placeholder="ID" require>
    

     <div style="justify-content: center;">
     <button class="btn-ghost red round" type="submit">Eliminar</button>
     </div>
     </form>
        </div>
      </section>


      <section>
  <!--for demo wrap-->
  <h1>Tabla de Productos</h1>
  <div class="tbl-header">
    <input type="text" name="input1" id="input1" class="input-style" placeholder="Buscar....">
    <table cellpadding="0" cellspacing="0" border="0">
      <thead>
        <tr>
          <th>Id</th>
          <th>Producto</th>
          <th>Descripcion</th>
          <th>Precio</th>
          <th>Existencias</th>
        </tr>
      </thead>
    </table>
  </div>
  <div class="tbl-content">
    <table cellpadding="0" cellspacing="0" border="0">
      <tbody id="tabla1">
        <?php 
        require '../action/conection.php';
        
        $consulta = mysqli_query($db, "SELECT * FROM productos");

        while($mostrar = mysqli_fetch_array($consulta)){
        
        
      
        ?>

     <tr>
       <td><?php 
       echo $mostrar["id"] ?></td>
       <td><?php echo $mostrar["producto"] ?></td>
       <td><?php echo $mostrar["descripcion"] ?></td>
       <td><?php echo $mostrar["precio"] ?></td>
       <td><?php echo $mostrar["stock"] ?></td>

     </tr>
     <?php 
     }
     ?>

      </tbody>
    </table>
  </div>
</section>


 
  </section>

  <script src="../js/script_slidebar.js"></script>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

 <script>
    $(document).ready(function(){
        $("#input1").on("keyup",function(){
            var value= $(this).val().toLowerCase()

            $('#tabla1 tr').filter(function(){
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            })
        })
    })
 </script>

</body>
</html>
